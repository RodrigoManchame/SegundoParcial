from .models import *
from rest_framework import serializers



class ClientesSerializer(serializers.ModelSerializer):
    class Meta:
        model = clientes
        fields = ['nombre', 'apellidos', 'dni', 'direccion','fecha_nacimiento','edad' ]