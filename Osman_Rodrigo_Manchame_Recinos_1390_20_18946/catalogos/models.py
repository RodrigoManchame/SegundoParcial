from django.db import models

# Create your models here.


class clientes(models.Model):
    nombre = models.CharField(max_length=50)
    apellidos =  models.CharField(max_length=30)
    dni = models.CharField(max_length=9)
    direccion = models.CharField(max_length=40)
    fecha_nacimiento = models.DateField()
    edad = models.CharField(max_length=3)

    class Meta:
        verbose_name = 'Clientes'
        verbose_name_plural = 'Clientes'

    def __str__(self):
        return self.nombre
    
