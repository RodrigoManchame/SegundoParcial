from django.contrib import admin
from .models import clientes


class ClientesAdmin(admin.ModelAdmin):
    list_display = ['nombre']

    