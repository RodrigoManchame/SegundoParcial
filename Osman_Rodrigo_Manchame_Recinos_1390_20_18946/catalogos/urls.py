from django.urls import path
from .views import *

urlpatterns = [
path('clientes/', ClientesListView.as_view(), name='clientes_list'),
path('clientes/nuevo/', ClientesCreateView.as_view(), name='clientes_create'),
path('clientes/<int:pk>/', ClientesUpdateView.as_view(), name='clientes_update'),
path('clientes/<int:pk>/eliminar/', ClientesDeleteView.as_view(), name='clientes_delete'),
]