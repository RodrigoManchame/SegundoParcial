from django import forms
from .models import clientes

class ClientesForm(forms.ModelForm):
    class Meta:
        model = clientes
        fields = ['nombre', 'apellidos', 'dni', 'direccion','fecha_nacimiento','edad' ]

