from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
# Create your views here.
from django.urls import reverse_lazy
from .models import clientes
from .forms import ClientesForm
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from .serializers import ClientesSerializer

class ClientesListView(LoginRequiredMixin, PermissionRequiredMixin,ListView):
    model = clientes
    template_name = 'clientes/clientes_list.html'
    context_object_name = 'clientes'
    login_url = reverse_lazy('login')
    permission_required = 'catalogos.view_clientes'

class ClientesCreateView(LoginRequiredMixin, PermissionRequiredMixin,CreateView):
    model = clientes
    form_class = ClientesForm
    template_name = 'clientes/clientes_form.html'
    success_url = reverse_lazy('clientes_list')
    login_url = reverse_lazy('login')
    permission_required = 'catalogos.add_clientes'

class ClientesUpdateView(LoginRequiredMixin, PermissionRequiredMixin,UpdateView):
    model = clientes
    form_class = ClientesForm
    template_name = 'clientes/clientes_form.html'
    success_url = reverse_lazy('clientes_list')
    login_url = reverse_lazy('login')

class ClientesDeleteView(LoginRequiredMixin, PermissionRequiredMixin,DeleteView):
    model = clientes
    template_name = 'clientes/clientes_confirm_delete.html'
    success_url = reverse_lazy('clientes_list')
    login_url = reverse_lazy('login')


class ClientesViewSet(viewsets.ModelViewSet):
    queryset = clientes.objects.all()
    serializer_class = ClientesSerializer
    authentication_classes = [TokenAuthentication, SessionAuthentication]
    permission_classes = [IsAuthenticated]

