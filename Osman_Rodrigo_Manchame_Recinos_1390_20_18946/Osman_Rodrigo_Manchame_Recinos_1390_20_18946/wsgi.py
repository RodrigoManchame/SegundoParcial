"""
WSGI config for Osman_Rodrigo_Manchame_Recinos_1390_20_18946 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Osman_Rodrigo_Manchame_Recinos_1390_20_18946.settings')

application = get_wsgi_application()
